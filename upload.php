<?php
session_start();
include "connection.php";
if(!isset($_SESSION['username']))
{
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>New Upload</title>
    <link rel="stylesheet" type="text/css" href="upload.css">
  </head>
  <body>
    <header>
      <div class="logo"><img src="gambar/dutabox.png"/></div>

      <div class="menu">
        <span><a href="index.php">Home</a></span>
        <span><a href="upload.php">Upload</a></span>
        <span><a href="about.php">About</a></span>
      </div>

      <div class="panel">
        <?php
        include "loginstatus.php";
        echo "<a>".$_SESSION['username']." </a>";
        if ($adminmode)
        {
          echo "<a>(Admin)</a>";
        }
        echo "<a class='logout_link' href='logout.php'>Logout</a>";
        ?>
        <input type="text" name="search" placeholder="search...">
        <button id="search-button">search</button>
      </div>

    </header>
    <div id="content">
      <h1>UPLOAD</h1>
      <form id ="form_upload" action="upload.php" method="post" enctype="multipart/form-data">
        <label for="judul">Judul : </label>
        <input id="input_judul" type="text" name="judul" placeholder="Judul"><br>

        <label for="pembuat">Pembuat : </label>
        <input id="input_pembuat" type="text" name="pembuat" placeholder="Nama Pembuat"><br>

        <label for="deskripsi">Deskripsi</label>
        <textarea id = "input_deskripsi" name="deskripsi" rows="8" cols="40" placeholder="Deskripsi"></textarea><br>

        <label for="kategori">Kategori</label>
        <select id = "input_kategori" name="kategori">
          <option selected hidden>Pilihlah</option>
          <option value="1">Animasi</option>
          <option value="2">Jaringan</option>
          <option value="3">Desain</option>
          <option value="4">Games</option>
          <option value="5">Lainnya</option>
        </select><br>

        <label for="file">Pilih Gambar</label>
        <input type="file" name="file">
        <br>
        <br>
        <input id = "unggah" type="submit" name="unggah" value="Upload">

        <br>
      </form>
      <?php
        if(!empty($_POST['unggah']))
        {
          $judul=$_POST['judul'];
          $pembuat=$_POST['pembuat'];
          $deskripsi=$_POST['deskripsi'];
          $kategori=$_POST['kategori'];

          $target_dir = "uploadedfiles/";
          $file = $_FILES['file'];
          $nama_file = $_FILES['file']['name'];
          $tmp_file = $_FILES['file']['tmp_name'];
          $path = $target_dir . $nama_file;

          move_uploaded_file($tmp_file, $path);

          $query = "INSERT INTO post(judul,pembuat,deskripsi,file,kategori) VALUES ('$judul','$pembuat','$deskripsi','$nama_file','$kategori')";

          if (mysqli_query($connection , $query))
          {
            echo "<a class = 'uploadwarning'>Data berhasil diupload! Silahkan cek homepage :D</a>";
          }
          else
          {
            echo "<a classs = 'uploadwarning'>Data gagal disimpan!</a>";
          }
        }
      ?>
    </div>

    <div id="terbaru">
      <div class="baru">

      </div>
      <div class="baru">

      </div>
      <div class="baru">

      </div>
      <div class="baru">

      </div>
      <div class="baru">

      </div>

    </div>

    <footer>
      <nav id="navkategori">
        <a class="kategori" href="" > Animasi </a>
        <a class="kategori" href="" > Jaringan </a>
        <a class="kategori" href="" > Desain </a>
        <a class="kategori" href="" > Games </a>
      </nav>
    </footer>
  </body>
</html>
