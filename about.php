<?php
session_start();
include "connection.php";
if(!isset($_SESSION['username']))
{
  header("location: login.php");
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>About Us</title>
    <link rel="stylesheet" type="text/css" href="about.css">
  </head>
  <body>
    <header>
      <div class="logo"><img src="gambar/dutabox.png"/></div>

      <div class="menu">
        <span><a href="index.php">Home</a></span>
        <span><a href="upload.php">Upload</a></span>
        <span><a href="about.php">About</a></span>
      </div>

      <div class="panel">
        <?php
        include "loginstatus.php";
        echo "<a>".$_SESSION['username']." </a>";
        if ($adminmode)
        {
          echo "<a>(Admin)</a>";
        }
        echo "<a class='logout_link' href='logout.php'>Logout</a>";
        ?>
        <input type="text" name="search" placeholder="search...">
        <button id="search-button">search</button>
      </div>

    </header>
    <div id="content">
      <h1>DUTA BOX</h1>
      <ul>
        Profil Admin:
        <li>Novandy Sutan (novandy99@gmail.com)</li>
        <li>Krisma Aditya (@krismaaditya)</li>
        <li>Ferdi Angga (ferdiangga@yahoo.com)</li>
        <li>Helviqi (helviqi@yahoo.com)</li>
      </ul>
      <ol>
        Cara Upload
        <li>Log in dengan memasukkan username dan password, jika belum memiliki akun silahkan daftar terlebih dahulu</li>
        <li>Untuk mendaftar klik menu "Sign up"</li>
        <li>Jika sudah log in pilih menu "Upload"</li>
        <li>Buat judul dan deskripsi tentang konten yang akan di-upload</li>
        <li>Pilih gambar preview dengan meng-klik tombol "Browse"</li>
        <li>Klik tombol "Upload", setelah itu konten yang tadi di-upload akan muncul di website</li>
      </ol>
    </div>

    <div id="terbaru">
      <div class="baru">

      </div>
      <div class="baru">

      </div>
      <div class="baru">

      </div>
      <div class="baru">

      </div>
      <div class="baru">

      </div>

    </div>

    <footer>
      <nav id="navkategori">
        <a class="kategori" href="" > Animasi </a>
        <a class="kategori" href="" > Jaringan </a>
        <a class="kategori" href="" > Desain </a>
        <a class="kategori" href="" > Games </a>
      </nav>
    </footer>
  </body>
</html>
