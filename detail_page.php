<?php
include "connection.php";
session_start();
if(!isset($_SESSION['username']))
{
  header("location: login.php");
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>

      <?php
      $id = $_GET['id_post'];
      $query = "SELECT * FROM post where id_post = $id";
      $hasil = mysqli_query($connection , $query);
      while ($data = mysqli_fetch_assoc($hasil))
        {
          echo $data['judul'];
        }
      ?>

  </title>
    <link rel="stylesheet" type="text/css" href="detail_page.css">
    <script src="js/jquery-2.1.1.min.js"></script>
  </head>
  <body>
    <header>
      <div class="logo"><img src="gambar/dutabox.png"/></div>

      <div class="menu">
        <span><a href="index.php">Home</a></span>
        <span><a href="upload.php">Upload</a></span>
        <span><a href="about.php">About</a></span>
      </div>

      <div class="panel">
        <?php
        include "loginstatus.php";
        echo "<a>".$_SESSION['username']." </a>";
        if ($adminmode)
        {
          echo "<a>(Admin)</a>";
        }
        echo "<a class='logout_link' href='logout.php'>Logout</a>";
        ?>
        <input type="text" name="search" placeholder="search...">
        <button id="search-button">search</button>
      </div>

    </header>

    <div id="content">
        <?php
        include "loginstatus.php";

        if ($adminmode)
        {
            echo "<div id = 'options'>
            <button id='option_button'>sunting</button>
            <a id='option' href='deletepost.php?id_post=$id'>Hapus</a>
            </div>";

            echo "<script>
            $(document).ready(function()
            {
              $('#option').hide();
              $('#option_button').click(function()
                {
                  $('#option').toggle();
                });
            });
            </script>";
        }

        $id = $_GET['id_post'];

        $query = "SELECT * FROM post where id_post = $id";
        $hasil = mysqli_query($connection , $query);

        while ($data = mysqli_fetch_assoc($hasil))
        {
          echo "<div class='gambar'><img src='uploadedfiles/".$data['file']."'></div>";
          echo "<h1 class='judul'>".$data['judul']."</h1>";
          echo "<p class='pembuat'>Pembuat : ".$data['pembuat']."</p>";
          echo "<p class='deskripsi'>Deskripsi : ".$data['deskripsi']."</p>";

          $kategori_query = "SELECT k.nama  FROM kategori k, post p WHERE k.nomor = p.kategori AND p.id_post = $id";
          $kategori = mysqli_query($connection , $kategori_query);

          $result = mysqli_fetch_assoc($kategori);

            echo "<p class='kategori'>Kategori : ".$result['nama']."</p>";
        }
         ?>
    </div>

    <div id="description">

    </div>

    <footer>
    </footer>
  </body>
</html>
