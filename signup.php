<?php
include "connection.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Sign up</title>
    <link rel="stylesheet" type="text/css" href="login_signup.css">
  </head>
  <body>

    <div id="wrapper">
      <h2>Sign up</h2>
      <div class="input">
        <form action="signup.php" method="post">
          <input type="text" name="username" placeholder="Username">
          <input type="password" name="password" placeholder="Password">
          <input type="text" name="email" placeholder="Email">
          <input type="text" name="nohp" placeholder="No.HP">
          <input type="submit" name="submit" value="Sign Up">
          <h6 class="tanya_akun">Sudah punya akun? Log in di <a href="login.php">sini</a></h6>
        </form>

          <?php
            if(isset($_POST['username']))
              {
                $username=$_POST['username'];
                $password=$_POST['password'];
                $email=$_POST['email'];
                $nohp=$_POST['nohp'];

                $username = stripslashes($username);
                $password = stripslashes($password);
                $email = stripslashes($email);
                $nohp = stripslashes($nohp);
                $username = mysqli_real_escape_string($connection, $username);
                $password = mysqli_real_escape_string($connection, $password);
                $email = mysqli_real_escape_string($connection, $email);
                $nohp = mysqli_real_escape_string($connection, $nohp);
                $password = sha1($password);

                if(!$username || !$password)
                    {
                      echo "<a class='warning'>Data masih kosong!</a>";
                    }
                    else
                    {
                      $sql = "INSERT INTO users (username, password, email, nohp) VALUES ('$username', '$password', '$email', '$nohp')";
                      mysqli_query($connection, $sql);
                      echo "<h4 style color='white'>Anda berhasil mendaftar, silahkan log in</h4>";
                    }
                  }

          mysqli_close($connection);
          ?>

      </div>

    </div>

  </body>
</html>
