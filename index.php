<?php
include "connection.php";
session_start();
if(!isset($_SESSION['username']))
{
  header("location: login.php");
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="index.css">
  </head>
  <body>
    <header>

      <div class="menu">
        <div class="logo"><a href = "index.php"><img src="gambar/dutabox.png"/></a></div>

        <div class="menu-left">
          <span><a href="index.php">Home</a></span>
          <span><a href="upload.php">Upload</a></span>
          <span><a href="about.php">About</a></span>
        </div>

        <div class="panel">

            <?php
            	if(isset($_SESSION['username']))
              {
                include "loginstatus.php";
                echo "<a class = 'username'>".$_SESSION['username']." </a>";
                if ($adminmode)
                {
                  echo "<a class = 'admin'>(Admin)</a>";
                }
                echo "<a class='logout_link' href='logout.php'>Logout</a>";
            	}
            ?>
            <form class="" action="search.php" method="get">
              <input type="text" id="search-field" name="search" placeholder="search...">
              <button id="search-button">search</button>
            </form>
        </div>

      </div>


    </header>

    <div id="content">
      <?php
      $cek = 0;

      $check = mysqli_query($connection , "SELECT count(id_post) FROM post");
      while ($data = mysqli_fetch_assoc($check))
      {
        $cek = $data['count(id_post)']."</br>";
        if ($cek == 0)
        {
          echo "Tidak ada postingan";
        }

        else {
          //echo "<h1>".$cek."</h1>";
        $query = "SELECT * FROM post";
        $hasil = mysqli_query($connection , $query);

        while ($data = mysqli_fetch_assoc($hasil))
        {
          echo
          "<a href = 'detail_page.php?id_post=".$data['id_post']."'>
          <div>
          <img class = 'thumbnails' src = 'uploadedfiles/".$data['file']."'>
          <h4 class = 'caption_thumbnails'>".$data['judul']."</h4>
          <p class = 'author'>oleh ".$data['pembuat']."</p>
          </div>
          </a>";
        }
        }
      }
      ?>
    </div>

    <div id="category">

    </div>

    <footer>
      <nav id="navkategori">
        <a class="kategori" href="" > Animasi </a>
        <a class="kategori" href="" > Jaringan </a>
        <a class="kategori" href="" > Desain </a>
        <a class="kategori" href="" > Games </a>
      </nav>
    </footer>
  </body>
</html>
